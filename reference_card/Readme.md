This folder contains the source for the Cache Refresh bidding reference card.

To edit it, you will need Scribus 1.5.3 or later and the following fonts:

* [The NetrunnerDB font](https://github.com/Alsciende/netrunnerdb-font)
* [Orbitron Black](https://www.theleagueofmoveabletype.com/orbitron)
* [Gillius ADF No. 2 Condensed](http://arkandis.tuxfamily.org/adffonts.html) (it's in the Gillius Collection download)

If you want to edit the hand symbol, you will also need Inkscape. Edit hand.svg, save a copy as eps, load the eps into the image in Scribus, then embed the image in Scribus.

The last page is there to work around [a Scribus bug](https://bugs.scribus.net/view.php?id=14898). You may need to manually update it. Just open the symbol in the symbol editor, copy the text frame from it, then paste the text frame in the last page.