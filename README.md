![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

This is a fansite for the Cache Refresh format for Android: Netrunner.

Current goals are:
* provide a place for players to get info about Cache Refresh without having to
wade through a PDF aimed at Tournament Organizers;
* provide a version of the rules with clarifications of the confusing bits
(primarily the bidding to start with);
* provide a single location for things we know about Cache Refresh that aren't
in the offical PDF.